<?php declare(strict_types=1);

namespace HuebertAccountAttributes\Core\Content\Media;

use HuebertAccountAttributes\Core\Content\Download\Aggregate\DownloadMedia\DownloadMediaDefinition;
use HuebertAccountAttributes\Core\Content\Download\DownloadDefinition;
use Shopware\Core\Content\Media\MediaDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Inherited;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ManyToManyAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;
use \Shopware\Core\Framework\DataAbstractionLayer\EntityExtension;

class MediaExtension extends EntityExtension
{
    public function extendFields(FieldCollection $collection): void
    {
        $collection->add(
            (new ManyToManyAssociationField(
                'download',
                DownloadDefinition::class,
                DownloadMediaDefinition::class,
                'sysea_media_id',
                'sysea_download_id'
            ))->addFlags(new Inherited())
        );
    }

    /**
     * @inheritDoc
     */
    public function getDefinitionClass(): string
    {
        return MediaDefinition::class;
    }
}