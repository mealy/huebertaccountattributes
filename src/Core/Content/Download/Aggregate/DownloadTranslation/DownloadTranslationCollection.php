<?php declare(strict_types=1);

namespace HuebertAccountAttributes\Core\Content\Download\Aggregate\DownloadTranslation;

use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;

/**
 * @method void                         add(DownloadTranslationEntity $entity)
 * @method void                         set(string $key, DownloadTranslationEntity $entity)
 * @method DownloadTranslationEntity[]    getIterator()
 * @method DownloadTranslationEntity[]    getElements()
 * @method DownloadTranslationEntity|null get(string $key)
 * @method DownloadTranslationEntity|null first()
 * @method DownloadTranslationEntity|null last()
 */
class DownloadTranslationCollection extends EntityCollection
{
    protected function getExpectedClass(): string
    {
        return DownloadTranslationEntity::class;
    }
}