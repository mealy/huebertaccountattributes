<?php declare(strict_types=1);

namespace HuebertAccountAttributes\Core\Content\Download\Aggregate\DownloadTranslation;

use HuebertAccountAttributes\Core\Content\Download\DownloadEntity;
use Shopware\Core\Framework\DataAbstractionLayer\TranslationEntity;

class DownloadTranslationEntity extends TranslationEntity
{
    /**
     * @var string
     */
    protected $downloadId;

    /**
     * @var string|null
     */
    protected $name;

    /**
     * @var DownloadEntity
     */
    protected $download;

    /**
     * @var string
     */
    protected $syseaDownloadId;

    /**
     * @var DownloadEntity
     */
    protected $syseaDownload;

    public function getDownloadId(): string {
        return $this->downloadId;
    }

    public function setDownloadId(string $downloadId): void {
        $this->downloadId = $downloadId;
    }

    public function getName(): ?string {
        return $this->name;
    }

    public function setName(string $name): void {
        $this->name = $name;
    }

    /**
     * @return DownloadEntity
     */
    public function getDownload(): DownloadEntity
    {
        return $this->download;
    }

    /**
     * @param DownloadEntity $download
     */
    public function setDownload(DownloadEntity $download): void
    {
        $this->download = $download;
    }

    /**
     * @return string
     */
    public function getSyseaDownloadId(): string
    {
        return $this->syseaDownloadId;
    }

    /**
     * @param string $syseaDownloadId
     */
    public function setSyseaDownloadId(string $syseaDownloadId): void
    {
        $this->syseaDownloadId = $syseaDownloadId;
    }

    /**
     * @return DownloadEntity
     */
    public function getSyseaDownload(): DownloadEntity
    {
        return $this->syseaDownload;
    }

    /**
     * @param DownloadEntity $syseaDownload
     */
    public function setSyseaDownload(DownloadEntity $syseaDownload): void
    {
        $this->syseaDownload = $syseaDownload;
    }
}