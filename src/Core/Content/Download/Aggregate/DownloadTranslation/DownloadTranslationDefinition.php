<?php declare(strict_types=1);

namespace HuebertAccountAttributes\Core\Content\Download\Aggregate\DownloadTranslation;

use HuebertAccountAttributes\Core\Content\Download\DownloadDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\StringField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;
use Shopware\Core\Framework\DataAbstractionLayer\EntityTranslationDefinition;
class DownloadTranslationDefinition extends EntityTranslationDefinition
{
    public const ENTITY_NAME = 'sysea_download_translation';

    public function getEntityName(): string
    {
        return self::ENTITY_NAME;
    }

    public function getEntityClass(): string
    {
        return DownloadTranslationEntity::class;
    }

    protected function getParentDefinitionClass(): string
    {
        return DownloadDefinition::class;
    }

    protected function defineFields(): FieldCollection
    {
        return new FieldCollection([
            (new StringField('name', 'name'))->addFlags(new Required())
        ]);
    }
}