<?php declare(strict_types=1);

namespace HuebertAccountAttributes\Core\Content\Download\Aggregate\DownloadMedia;

use HuebertAccountAttributes\Core\Content\Download\DownloadDefinition;
use Shopware\Core\Content\Media\MediaDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\CreatedAtField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\FkField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\PrimaryKey;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ManyToOneAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;
use Shopware\Core\Framework\DataAbstractionLayer\MappingEntityDefinition;

class DownloadMediaDefinition extends MappingEntityDefinition
{
    public const ENTITY_NAME = "sysea_download_media";

    public function getEntityName(): string
    {
        return self::ENTITY_NAME;
    }

    protected function defineFields(): FieldCollection
    {
        return new FieldCollection([
            (new FkField('sysea_download_id', 'downloadId', DownloadDefinition::class))->addFlags(new PrimaryKey(), new Required()),
            (new FkField('sysea_media_id', 'mediaId', MediaDefinition::class))->addFlags(new PrimaryKey(), new Required()),
            new ManyToOneAssociationField('download', 'sysea_download_id', DownloadDefinition::class),
            new ManyToOneAssociationField('media', 'sysea_media_id', MediaDefinition::class),
            new CreatedAtField()
        ]);
    }
}