<?php declare(strict_types=1);

namespace HuebertAccountAttributes\Core\Content\Download;

use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;

/**
 * @method void              add(DownloadEntity $entity)
 * @method void              set(string $key, DownloadEntity $entity)
 * @method DownloadEntity[]    getIterator()
 * @method DownloadEntity[]    getElements()
 * @method DownloadEntity|null get(string $key)
 * @method DownloadEntity|null first()
 * @method DownloadEntity|null last()
 */
class DownloadCollection extends EntityCollection
{
 protected function getExpectedClass(): string
 {
     return DownloadEntity::class;
 }
}