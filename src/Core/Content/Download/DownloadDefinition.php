<?php declare(strict_types=1);

namespace HuebertAccountAttributes\Core\Content\Download;

use Shopware\Core\Content\Media\MediaDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\PrimaryKey;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IdField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ManyToManyAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\StringField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\TranslatedField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\TranslationsAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;
use Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition;
use HuebertAccountAttributes\Core\Content\Download\Aggregate\DownloadMedia\DownloadMediaDefinition;
use HuebertAccountAttributes\Core\Content\Download\Aggregate\DownloadTranslation\DownloadTranslationDefinition;

class DownloadDefinition extends EntityDefinition
{
    public const ENTITY_NAME = "sysea_download";

    public function getEntityName(): string
    {
        return self::ENTITY_NAME;
    }

    public function getEntityClass(): string
    {
        return DownloadEntity::class;
    }

    protected function defineFields(): FieldCollection
    {
        return new FieldCollection([
            (new IdField('id', 'id'))->addFlags(new Required(), new PrimaryKey()),
            (new TranslatedField('name'))->addFlags(new Required()),
            (new StringField('customerGroups', 'customerGroups')),
            new TranslationsAssociationField(DownloadTranslationDefinition::class, 'sysea_download_id'),
            (new ManyToManyAssociationField('medias', MediaDefinition::class, DownloadMediaDefinition::class, 'sysea_download_id', 'sysea_media_id'))
        ]);
    }
}