<?php declare(strict_types=1);

namespace HuebertAccountAttributes\Core\Content\Download;

use HuebertAccountAttributes\Core\Content\Download\Aggregate\DownloadTranslation\DownloadTranslationCollection;
use Shopware\Core\Content\Media\MediaCollection;
use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityIdTrait;

class DownloadEntity extends Entity
{
    use EntityIdTrait;

    /**
     * @var string
     */
    protected $downloadId;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var MediaCollection
     */
    protected $medias;

    /**
     * @var string
     */
    protected $customerGroups;

    /**
     * @var DownloadTranslationCollection
     */
    protected $translations;

    /**
     * @var string
     */
    protected $syseaDownloadId;

    /**
     * @return string
     */
    public function getDownloadId(): string
    {
        return $this->downloadId;
    }

    /**
     * @param string $downloadId
     */
    public function setDownloadId(string $downloadId): void
    {
        $this->downloadId = $downloadId;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return MediaCollection
     */
    public function getMedias(): MediaCollection
    {
        return $this->medias;
    }

    /**
     * @param MediaCollection $medias
     */
    public function setMedias(MediaCollection $medias): void
    {
        $this->medias = $medias;
    }

    /**
     * @return string|null
     */
    public function getCustomerGroups(): ?string
    {
        return $this->customerGroups;
    }

    /**
     * @param string $customerGroups
     */
    public function setCustomerGroups(string $customerGroups): void
    {
        $this->customerGroups = $customerGroups;
    }

    /**
     * @return DownloadTranslationCollection
     */
    public function getTranslations(): DownloadTranslationCollection
    {
        return $this->translations;
    }

    /**
     * @param DownloadTranslationCollection $translations
     */
    public function setTranslations(DownloadTranslationCollection $translations): void
    {
        $this->translations = $translations;
    }

    /**
     * @return string
     */
    public function getSyseaDownloadId(): string
    {
        return $this->syseaDownloadId;
    }

    /**
     * @param string $syseaDownloadId
     */
    public function setSyseaDownloadId(string $syseaDownloadId): void
    {
        $this->syseaDownloadId = $syseaDownloadId;
    }


}