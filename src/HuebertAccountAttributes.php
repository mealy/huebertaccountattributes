<?php declare(strict_types=1);

namespace HuebertAccountAttributes;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\DataAbstractionLayer\Indexing\EntityIndexerRegistry;
use Shopware\Core\Framework\Plugin;
use Shopware\Core\Framework\Plugin\Context\ActivateContext;
use Shopware\Core\Framework\Plugin\Context\InstallContext;
use Shopware\Core\Framework\Plugin\Context\UninstallContext;

class HuebertAccountAttributes extends Plugin
{
    public function activate(ActivateContext $activateContext): void
    {
        $registry = $this->container->get(EntityIndexerRegistry::class);
        $registry->sendIndexingMessage(['media.indexer']);
    }

    public function postInstall(InstallContext $installContext): void
    {
        $connection = $this->container->get(Connection::class);
        $connection->executeUpdate('UPDATE media SET download = id WHERE download IS NULL');
    }

    public function uninstall(UninstallContext $uninstallContext): void
    {
        if($uninstallContext->keepUserData()) {
            return;
        }

        $connection = $this->container->get(Connection::class);

        $connection->executeUpdate('DROP TRIGGER IF EXISTS `update_media_download`');
        $connection->executeUpdate('ALTER TABLE media DROP COLUMN download;');
        $connection->executeUpdate('DROP TABLE sysea_download_media;');
        $connection->executeUpdate('DROP TABLE sysea_download_translation;');
        $connection->executeUpdate('DROP TABLE sysea_download;');
    }
}