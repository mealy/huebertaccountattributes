<?php declare(strict_types=1);

namespace HuebertAccountAttributes\Controller;

use HuebertAccountAttributes\Core\Content\Download\DownloadEntity;
use Shopware\Core\Content\Media\MediaCollection;
use Shopware\Core\Content\Media\MediaEntity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\Routing\Annotation\RouteScope;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Storefront\Controller\StorefrontController;
use Shopware\Storefront\Page\GenericPageLoader;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @RouteScope(scopes={"storefront"})
 */
class DownloadController extends StorefrontController
{
    /**
     * @var GenericPageLoader $pageLoader
     */
    private $pageLoader;

    /**
     * @var EntityRepositoryInterface
     */
    private $downloadRepository;

    /**
     * @var EntityRepositoryInterface
     */
    private $mediaRepository;

    public function __construct(
        GenericPageLoader $pageLoader,
        EntityRepositoryInterface $downloadRepository,
        EntityRepositoryInterface $mediaRepository
    )
    {
        $this->pageLoader = $pageLoader;
        $this->downloadRepository = $downloadRepository;
        $this->mediaRepository = $mediaRepository;
    }

    /**
     * @Route("/download/{name}", name="frontend.download.page", methods={"GET"})
     */
    public function downloadOverview(Request $request, SalesChannelContext $salesChannelContext): Response {
        $context = $salesChannelContext->getContext();
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('name', $request->get('name')));
        $downloads = $this->downloadRepository->search($criteria, $context)->getEntities();

        /**
         * @var DownloadEntity
         */
        $download = $downloads->first();
        $customerGroups = [];
        $forAll = false;

        if(empty($this->getCustomerGroupsArray($download))) {
            $forAll = true;
        } else {
            foreach($this->getCustomerGroupsArray($download) as $group) {
                $customerGroups[] = $group;
            }
        }
        
        if(!$forAll) {
            if(empty($salesChannelContext->getCustomer()) || !in_array($salesChannelContext->getCurrentCustomerGroup()->getName(), $customerGroups)) {
                    return new RedirectResponse('/account', 302);
            }
        }

        $page = $this->pageLoader->load($request, $salesChannelContext);
        $pageName = $request->get('name');
        $page->assign([
            'huebert_active' => $pageName,
            'huebertDownloadEntities' => $this->getDownloads($pageName, $salesChannelContext->getContext())
        ]);
        return $this->renderStorefront('@HuebertAccountAttributes/storefront/page/download/index.html.twig',
            [
                'page' => $page,
            ]
        );
    }

    private function getDownloads($pageName, $context) {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('name', $pageName));
        $criteria->addAssociation('medias');
        $downloads = $this->downloadRepository->search($criteria, $context)->getElements();
        $mediaCollection = new MediaCollection();
        /**
         * @var DownloadEntity $download
         */
        foreach($downloads as $download) {
            $medias = $download->getMedias();

            foreach($medias as $media) {
                $mediaCriteria = new Criteria();
                $mediaCriteria->addFilter(new EqualsFilter('id', $media->getId()));
                $mediaCriteria->addAssociation('url');
                /**
                 * @var MediaEntity $mediaResult
                 */
                $mediaResult = $this->mediaRepository->search($mediaCriteria, $context)->getEntities()->first();
                $media->assign([
                    'url' => $mediaResult->getUrl()
                ]);
                $mediaCollection->add($media);
            }
        }

        return $mediaCollection;
    }

    /**
     * @param DownloadEntity $download
     * @return array
     */
    private function getCustomerGroupsArray($download): ?array {
        $customerGroups = $download->getCustomerGroups();
        if(empty($customerGroups)) {
            return null;
        }

        $customerGroupsArray = [];

        if(strpos($customerGroups, ",") !== false) {
            $customerGroupsExplode = explode(",", $customerGroups);
            foreach($customerGroupsExplode as $customerGroup) {
                $customerGroupsArray[] = ltrim(trim($customerGroup));
            }
            return $customerGroupsArray;
        } else {
            $customerGroupsArray[] = ltrim(trim($customerGroups));
            return $customerGroupsArray;
        }
    }
}