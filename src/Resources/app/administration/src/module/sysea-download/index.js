import './page/sysea-download-list';
import './page/sysea-download-detail';
import './page/sysea-download-create';
import './components/huebert-download-icon';
import deDE from './snippet/de-DE.json';
import enGB from './snippet/en-GB.json';

Shopware.Module.register('sysea-download', {
    type: 'plugin',
    name: 'Download',
    title: 'sysea-download.general.mainMenuItemGeneral',
    description: 'sysea-download.general.descriptionTextModule',
    color: '#6c63ff',
    icon: 'default-shopping-paper-bag-product',

    snippets: {
        'de-DE': deDE,
        'en-GB': enGB
    },

    routes: {
        list: {
            component: 'sysea-download-list',
            path: 'list'
        },
        detail: {
            component: 'sysea-download-detail',
            path: 'detail/:id',
        },
        create: {
            component: 'sysea-download-create',
            path: 'create',
        },
    },

    settingsItem: {
        group: 'plugins',
        name: 'sysea-download',
        to: 'sysea.download.list',
        label: 'sysea-download.general.mainMenuItemGeneral',
        backgroundEnabled: false,
        iconComponent: 'huebert-download-icon'
    }
});