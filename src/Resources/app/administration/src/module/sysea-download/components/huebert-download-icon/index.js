import template from './huebert-download-icon.html.twig';

const { Component } = Shopware;

Component.register('huebert-download-icon', {
    template
});
