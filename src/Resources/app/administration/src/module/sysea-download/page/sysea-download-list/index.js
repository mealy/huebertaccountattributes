import template from './sysea-download-list.html.twig';

const Criteria = Shopware.Data.Criteria;

Shopware.Component.register('sysea-download-list', {
    template,

    inject: [
        'repositoryFactory'
    ],

    data() {
        return {
            repository: null,
            download: null,
            isLoading: false
        }
    },

    created() {
        if (!Shopware.State.getters['context/isSystemDefaultLanguage']) {
            Shopware.State.commit('context/resetLanguageToDefault');
        }

        this.repository = this.repositoryFactory.create('sysea_download');

        this.repository
            .search(new Criteria(), Shopware.Context.api)
            .then((res) => {
                this.download = res;
            })
    },

    computed: {
        columns() {
            return [{
                property: 'name',
                dataIndex: 'name',
                label: this.$tc('sysea-download.list.name'),
                routerLink: 'sysea.download.detail',
                inlineEdit: 'string',
                allowResize: true,
                primary: true
            }, {
                property: 'customerGroups',
                dataIndex: 'customerGroups',
                label: this.$tc('sysea-download.list.customerGroups'),
                routerLink: 'sysea.download.detail',
                allowResize: true,
                primary: false
            }];
        }
    },

    methods: {
        onChangeLanguage(languageId) {
            Shopware.State.commit('context/setApiLanguageId', languageId);
            this.getList(languageId);
        },

        getList(languageId) {
            this.isLoading = true;
            let criteria = new Criteria();
            if(languageId) {
                criteria.addFilter(
                    Criteria.equals('translations.languageId', languageId)
                )
            }
            this.repository
                .search(criteria, Shopware.Context.api)
                .then((res) => {
                    this.download = res;
                    this.isLoading = false;
                });
        },
    },

    metaInfo() {
        return {
            title: this.$createTitle()
        }
    }
});