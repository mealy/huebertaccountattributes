import template from './sysea-download-detail.html.twig';

const Mixin = Shopware.Mixin;
const { warn } = Shopware.Utils.debug;
const Criteria = Shopware.Data.Criteria;

Shopware.Component.register('sysea-download-detail', {
    template,

    provide() {
        return {
            setActiveItemIndex: this.setActiveItemIndex
        };
    },

    inject: [
        'repositoryFactory',
        'setActiveItemIndex'
    ],

    mixins: [
        Mixin.getByName('notification')
    ],

    metaInfo() {
        return {
            title: this.$createTitle()
        };
    },

    mounted() {
        this.mountedComponent();
    },

    data() {
        return {
            download: null,
            isLoading: false,
            processSuccess: false,
            repository: null,
            activeItemIndex: 0,
            medias: [],
            isSaveSuccessful: false,
            downloadId: null,
            columnCount: 5,
            columnWidth: 90,
            disabled: false,
            languageDisabled: false
        }
    },

    computed: {
        downloadMediaRepository() {
            return this.repositoryFactory.create('sysea_download_media');
        },

        mediaRepository() {
            return this.repositoryFactory.create('media');
        },

        downloadMedia() {
            if (!this.download) {
                return [];
            }
            return this.download.medias;
        },

        gridAutoRows() {
            return `grid-auto-rows: ${this.columnWidth}`;
        }
    },

    created() {
        this.repository = this.repositoryFactory.create('sysea_download');
        this.createdComponent();
    },

    methods: {
        createdComponent() {
            if (!this.download) {
                this.languageDisabled = true;
                this.disabled = true;
                // set language to system language
                if (!Shopware.State.getters['context/isSystemDefaultLanguage']) {
                    Shopware.State.commit('context/resetLanguageToDefault');
                }
                this.initializeContent();
            } else {
                if(this.download.name) {
                    this.disabled = false;
                }
            }
        },

        initializeContent() {
            this.getDownload();
        },

        getDownload() {
            if(!this.download) {
                let criteria = new Criteria();
                criteria.addFilter(Criteria.equals('id', this.$route.params.id));
                criteria.addAssociation('medias');
                this.repository.search(criteria, Shopware.Context.api)
                    .then((res) => {
                        this.download = res.first();
                        this.languageDisabled = this.download.isNew();
                        if(this.download.name) {
                            this.disabled = false;
                        }
                    }).catch((rej) => {
                        console.log("rejected", rej);
                });
            } else {
                if(!this.download.name || this.download.name == "") {
                    this.disabled = true
                }
                if(this.download.name) {
                    this.disabled = false;
                }
            }
        },

        showMedias() {
            if(this.download.medias) {
                this.download.medias.forEach((mediaItem) => {
                    this.syseaAddMedia(mediaItem).then((mediaId) => {
                        this.$root.$emit('media-added', mediaId);
                        return true;
                    }).catch(() => {
                        this.createNotificationError({
                            title: this.$tc('sysea-download.detail.saveErrorTitle'),
                            message: this.$tc('sysea-download.detail.duplicatedError')
                        });

                        return false;
                    });
                });
            }
        },

        openMediaSidebar() {
            this.$refs.mediaSidebarItem.openContent();
        },

        updateMediaDownloadAssociation(media) {
            this.isLoading = true;
            let mediaRes = this.mediaRepository.get(media.id, Shopware.Context.api)
                .then((res) => {
                    let foundMedia = res;
                    foundMedia.download = foundMedia.id;
                    this.mediaRepository.save(foundMedia, Shopware.Context.api)
                        .then((res) => {
                            this.isLoading = false;
                        }).catch((rej) => {
                            console.log("save error", rej);
                    });
                }).catch((rej) => {
                   console.log("Can't get media", rej);
                });
            this.isLoading = false;
        },

        createMediaAssociation(media) {
            const downloadMedia = this.downloadMediaRepository.create(Shopware.Context.api);

            downloadMedia.id = media.id;
            downloadMedia.download = media.id;
            downloadMedia.downloadId = this.download.id;
            downloadMedia.mediaId = media.targetId;

            if (this.download.medias.length <= 0) {
                downloadMedia.position = 0;
            } else {
                downloadMedia.position = this.download.medias.length;
            }
            return downloadMedia;
        },

        successfulUpload(media) {
            // on replace
            if (this.download.medias.find((downloadMedia) => downloadMedia.mediaId === media.id)) {
                this.createNotificationError({
                    message: this.$tc('sysea-download.detail.mediaAlreadyExists')
                });
                return;
            }

            this.updateMediaDownloadAssociation(media);

            const downloadMedia = this.createMediaAssociation(media);
            this.download.medias.add(downloadMedia);
        },

        onUploadFailed(uploadTask) {
            const toRemove = this.download.medias.find((downloadMedia) => {
                return downloadMedia.mediaId === uploadTask.targetId;
            });
            if (toRemove) {
                this.download.medias.remove(toRemove.id);
            }
            this.isLoading = false;
        },

        _checkIfMediaIsAlreadyUsed(mediaId) {
            return this.download.medias.some((downloadMedia) => {
                return downloadMedia.mediaId === mediaId;
            });
        },

        onAddItemToDownload(mediaItem) {
            if (this._checkIfMediaIsAlreadyUsed(mediaItem.id)) {
                this.createNotificationError({
                    message: this.$tc('sysea-download.detail.mediaAlreadyExists')
                });
                return false;
            }

            this.syseaAddMedia(mediaItem).then((mediaId) => {
                this.$root.$emit('media-added', mediaId);
                return true;
            }).catch(() => {
                this.createNotificationError({
                    title: this.$tc('sysea-download.detail.saveErrorTitle'),
                    message: this.$tc('sysea-download.detail.duplicatedError')
                });

                return false;
            });
            return true;
        },

        syseaAddMedia(mediaItem) {
            if (this.download.medias.has(mediaItem.id)) {
                // eslint-disable-next-line prefer-promise-reject-errors
                return Promise.reject('A media item with this id exists');
            }

            return this.mediaRepository.get(mediaItem.id, Shopware.Context.api).then((media) => {
                this.download.medias.add(media);
                return media.id;
            });

        },

        removeFile(downloadMedia) {
            // remove cover id if mediaId matches
            this.download.medias.remove(downloadMedia.id);
        },

        onMediaUploadButtonOpenSidebar() {
            this.$emit('open-sidebar');
        },
        
        abortOnLanguageChange() {
            return this.repository.hasChanges(this.download);
        },

        /*onSave() {
            this.isLoading = true;

            this.repository.save(this.download, Shopware.Context.api).then(() => {
                this.isLoading = false;
                this.isSaveSuccessful = true;
                if (this.download.id === null) {
                    this.$router.push({ name: 'sysea.download.detail', params: { id: this.download.id } });
                    return;
                }

                this.loadEntityData();
                this.showMedias();
            }).catch((rej) => {
                this.isLoading = false;

                this.createNotificationError({
                    title: this.$tc('sysea-download.detail.saveErrorTitle'),
                    message: rej
                });
            });


        },*/

        onItemRemove(mediaItem, index) {
            this.download.medias.remove(mediaItem.id);
            this.getDownload();
        },

        saveOnLanguageChange() {
            return this.onClickSave();
        },

        loadEntityData() {
            this.isLoading = true;
            let criteria = new Criteria();
            criteria.addAssociation('medias');
            return this.repository.get(this.download.id, Shopware.Context.api, criteria).then((download) => {
                this.isLoading = false;
                this.download = download;
                this.download.medias = download.medias;
                this.downloadId = this.download.id;
                this.disabled = this.download.name == "";
            });
        },

        getPlaceholderCount(columnCount) {
            if (this.downloadMedia.length + 3 < columnCount * 2) {
                columnCount *= 2;
            }

            let placeholderCount = columnCount;

            if (this.downloadMedia.length !== 0) {
                placeholderCount = columnCount - ((this.downloadMedia.length) % columnCount);
                if (placeholderCount === columnCount) {
                    return 0;
                }
            }

            return placeholderCount;
        },

        onMediaItemDragSort(dragData, dropData, validDrop) {
            if (validDrop !== true) {
                return;
            }
            this.download.medias.moveItem(dragData.position, dropData.position);

            this.updateMediaItemPositions();
        },

        updateMediaItemPositions() {
            this.downloadMedia.forEach((medium, index) => {
                medium.position = index;
            });
        },

        getEntityName() {
          return 'sysea_download';
        },

        onChangeLanguage(languageId) {
            Shopware.State.commit('context/setApiLanguageId', languageId);
            this.loadEntityData();
        },

        onClickSave() {
            this.isLoading = true;
            this.repository
                .save(this.download, Shopware.Context.api)
                .then(() => {
                    this.getDownload();
                    this.isLoading = false;
                    this.processSuccess = true;
                    this.createNotificationSuccess({
                        title: this.$tc('sysea-download.detail.saveSuccessTitle'),
                        message: this.$tc('sysea-download.detail.saveSuccessMessage')
                    });
                })
                .catch((rej) => {
                    this.isLoading = false;
                    this.createNotificationError({
                        title: this.$tc('sysea-download.detail.saveErrorTitle'),
                        message: rej
                    });
                });
        },
        onMouseEnter() {
            this.setActiveItemIndex(this.index);
        },

        setIndexEnter() {
            this.setActiveItemIndex(this.index);
        },

        mountedComponent() {
            // Set first item active
            this.emitActiveItemIndex();
        },

        setActiveItemIndex(index) {
            this.activeItemIndex = index;
            this.emitActiveItemIndex();
        },

        emitActiveItemIndex() {
            this.$emit('active-item-change', this.activeItemIndex);
        },

        saveFinish() {
            this.processSuccess = false;
        },

        checkIfFilled(a) {
            this.disabled = this.download.name === "";
        }
    }

});
