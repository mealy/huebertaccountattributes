Shopware.Component.extend('sysea-download-create', 'sysea-download-detail', {
    methods: {
        getDownload() {
            this.download = this.repository.create(Shopware.Context.api);
        },

        onClickSave() {
            this.isLoading = true;

            this.repository
                .save(this.download, Shopware.Context.api)
                .then((res) => {
                    this.createNotificationSuccess({
                        title: this.$tc('sysea-download.detail.saveSuccessTitle'),
                        message: this.$tc('sysea-download.detail.saveSuccessMessage')
                    });
                    this.isLoading = false;
                    this.$router.push({
                        name: 'sysea.download.detail',
                        params: {
                            id: this.download.id
                        }
                    });
                })
                .catch((rej) => {
                    this.isLoading = false;

                    this.createNotificationError({
                        title: this.$tc('sysea-download.detail.saveErrorTitle'),
                        message: rej
                    });
                });
        },

        onChangeLanguage(languageId) {
            Shopware.State.commit('context/setApiLanguageId', languageId);
            this.getDownload();
        },
    }
});