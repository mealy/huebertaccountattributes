<?php declare(strict_types=1);

namespace HuebertAccountAttributes\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1619004579downloadTranslation extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1619004579;
    }

    public function update(Connection $connection): void
    {
        $connection->executeUpdate("
            CREATE TABLE IF NOT EXISTS `sysea_download_translation` (
                `sysea_download_id` BINARY(16) NOT NULL,
                `language_id` BINARY(16) NOT NULL,
                `name` VARCHAR(255) DEFAULT '' NOT NULL,
                `created_at` DATETIME(3) NOT NULL,
                `updated_at` DATETIME(3) NULL,
                PRIMARY KEY (`sysea_download_id`, `language_id`),
                CONSTRAINT `fk.download_translation.download_id` FOREIGN KEY (`sysea_download_id`)
                    REFERENCES `sysea_download` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                CONSTRAINT `fk.download_translation.language_id` FOREIGN KEY (`language_id`)
                    REFERENCES `language` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
        ");
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }
}
