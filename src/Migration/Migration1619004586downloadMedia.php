<?php declare(strict_types=1);

namespace HuebertAccountAttributes\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\InheritanceUpdaterTrait;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1619004586downloadMedia extends MigrationStep
{
    use InheritanceUpdaterTrait;

    public function getCreationTimestamp(): int
    {
        return 1619004586;
    }

    public function update(Connection $connection): void
    {
        $connection->executeUpdate("
            CREATE TABLE IF NOT EXISTS `sysea_download_media` (
                `sysea_download_id` BINARY(16) NOT NULL,
                `sysea_media_id` BINARY(16) NOT NULL,
                `created_at` DATETIME(3) NOT NULL,
                PRIMARY KEY (`sysea_download_id`, `sysea_media_id`),
            
                CONSTRAINT `fk.download_media.sysea_download_id` FOREIGN KEY (`sysea_download_id`)  
                    REFERENCES `sysea_download` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                CONSTRAINT `fk.download_media.sysea_media_id` FOREIGN KEY (`sysea_media_id`)  
                    REFERENCES `media` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
                
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
        ");

        $connection->executeUpdate('DROP TRIGGER IF EXISTS `update_media_download`');

        $connection->executeUpdate('
            CREATE TRIGGER update_media_download
            AFTER INSERT ON sysea_download_media
            FOR EACH ROW
            UPDATE media SET `download` = `id` WHERE `download` IS NULL OR `download` = UNHEX("00000000000000000000000000000000");
        ');

        $mediaCheck = $connection->executeQuery("
            SHOW COLUMNS FROM `media` LIKE 'download'
        ");

        $downloadExists = (bool)$mediaCheck->rowCount();

        if(!$downloadExists) {
            $this->updateInheritance($connection, 'media', 'download');
        }
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }
}
