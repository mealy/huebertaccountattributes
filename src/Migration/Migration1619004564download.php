<?php declare(strict_types=1);

namespace HuebertAccountAttributes\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1619004564download extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1619004564;
    }

    public function update(Connection $connection): void
    {
        $connection->executeUpdate("
            CREATE TABLE IF NOT EXISTS `sysea_download` (
                `id` BINARY(16) NOT NULL,
                `name` VARCHAR(255) DEFAULT '' NOT NULL,
                `customerGroups` TEXT DEFAULT '' NULL,
                `created_at` DATETIME(3) NOT NULL,
                `updated_at` DATETIME(3) NULL,
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
        ");
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }
}
